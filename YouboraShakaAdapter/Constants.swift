//
//  Constants.swift
//  YouboraShakaAdapter
//
//  Created by Elisabet Massó on 1/10/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation

final class Constants {
    
    static private var shakaAdapterVersion = "6.6.0"
    
    static func getAdapterName() -> String {
        return "\(getName())-\(getPlatform())"
    }
    
    static func getAdapterVersion() -> String {
        return "\(getVersion())-\(getAdapterName())"
    }
    
    static func getAdsAdapterName() -> String {
        return "\(getName())-Ads-\(getPlatform())"
    }
    
    static func getAdsAdapterVersion() -> String {
        return "\(getVersion())-\(getAdsAdapterName())"
    }
    
    static func getName() -> String {
        return "Shaka"
    }
    
    static func getPlatform() -> String {
        #if os(tvOS)
        return "tvOS"
        #else
        return "iOS"
        #endif
    }
    
    static func getVersion() -> String {
        return shakaAdapterVersion
    }
    
}
