//
//  YBShakaAdapter.swift
//  YouboraShakaAdapter
//
//  Created by Elisabet Massó on 2/3/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import YouboraLib
import ShakaPlayerEmbedded

public class YBShakaAdapter: YBPlayerAdapter<AnyObject>, ShakaPlayerClient {
    
    private var delegate: ShakaPlayerClient?
    
    private override init() {
        super.init()
    }
    
    public init(player: ShakaPlayer, client: ShakaPlayerClient?) {
        super.init(player: player)
        delegate = client
        registerListeners()
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? ShakaPlayer {
            player.client = self
        }
        monitorPlayhead(withBuffers: false, seeks: true, andInterval: 800)
    }
    
    public override func unregisterListeners() {
        delegate = nil
        monitor?.stop()
        super.unregisterListeners()
    }
    
    // MARK: - Getters
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getPlayerName() -> String? {
        return Constants.getAdapterName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdapterVersion()
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let player = player as? ShakaPlayer, let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) else { return nil }
        return NSNumber(floatLiteral: player.currentTime)
    }
    
    public override func getPlayrate() -> NSNumber {
        if flags.paused {
            return 0
        } else {
            return 1
        }
    }
    
    public override func getDuration() -> NSNumber? {
        guard let player = player as? ShakaPlayer else { return nil }
        return NSNumber(floatLiteral: player.duration)
    }
    
    public override func getRendition() -> String? {
        guard let width = getActiveTrack()?.width?.int32Value, let height = getActiveTrack()?.height?.int32Value, let bandwidth = getActiveTrack()?.bandwidth else { return super.getRendition() }
        return YBYouboraUtils.buildRenditionString(withWidth: width, height: height, andBitrate: bandwidth)
    }
    
    public override func getBitrate() -> NSNumber? {
        guard let player = player as? ShakaPlayer else { return super.getBitrate() }
        let stats = player.getStats()
        if !stats.streamBandwidth.isInfinite && !stats.streamBandwidth.isNaN {
            return NSNumber(floatLiteral: stats.streamBandwidth)
        }
        if let bandwidth = stats.switchHistory.last?.bandwidth?.doubleValue, !bandwidth.isInfinite && !bandwidth.isNaN {
            return NSNumber(floatLiteral: bandwidth)
        }
        return super.getBitrate()
    }
    
    public override func getIsLive() -> NSValue? {
        guard let player = player as? ShakaPlayer else { return nil }
        return NSNumber(booleanLiteral: player.isLive ? true : (getDuration() != nil))
    }
    
    public override func getThroughput() -> NSNumber? {
        guard let player = player as? ShakaPlayer else { return super.getThroughput() }
        return NSNumber(floatLiteral: player.getStats().estimatedBandwidth)
    }
    
    public override func getFramesPerSecond() -> NSNumber? {
        return getActiveTrack()?.frameRate
    }
    
    public override func getDroppedFrames() -> NSNumber? {
        guard let player = player as? ShakaPlayer else { return nil }
        return NSNumber(floatLiteral: player.getStats().droppedFrames)
    }
    
    func getActiveTrack() -> ShakaTrack? {
        guard let player = player as? ShakaPlayer else { return nil }
        var ret: ShakaTrack?
        if player.getVariantTracks().count > 0 {
            for track in player.getVariantTracks() where (track.type == "video" || track.type == "variant") && track.active {
                ret = track
            }
        } else if player.getTextTracks().count > 0 {
            for track in player.getTextTracks() where track.active {
                ret = track
            }
        }
        return ret
        
    }
    
    // MARK: - ShakaPlayerClient
    
    public func onPlayer(_ player: ShakaPlayer, error: ShakaPlayerError) {
        delegate?.onPlayer?(player, error: error)
        
        var msg = error.message
        if error.category < 10 && error.category > 0 {
            let typeDicc: [Int:String] = [ 1: "network", 2: "text", 3: "media", 4: "manifest", 5: "streaming", 6: "drm", 7: "player", 8: "cast", 9: "storage" ]
            msg = typeDicc[error.category] ?? msg
        }
        if error.severity == 2 && [1002,3016].firstIndex(of: error.code) != nil {
            fireFatalError(withMessage: msg, code: "\(error.code)", andMetadata: nil) // code is not on the list and severity is 2: fatal
        } else {
            fireError(withMessage: msg, code: "\(error.code)", andMetadata: nil) // not severity 2, or in the list: nonfatal
        }
    }
    
    public func onPlayer(_ player: ShakaPlayer, bufferingChange is_buffering: Bool) {
        delegate?.onPlayer?(player, bufferingChange: is_buffering)
        if is_buffering {
            fireBufferBegin()
        } else {
            fireBufferEnd()
        }
    }
    
    public func onPlayerPlayingEvent(_ player: ShakaPlayer) {
        delegate?.onPlayerPlayingEvent?(player)
        fireResume()
        fireStart()
        fireJoin()
    }
    
    public func onPlayerPauseEvent(_ player: ShakaPlayer) {
        delegate?.onPlayerPauseEvent?(player)
        if flags.joined && player.currentTime != 0 && player.playbackRate != 0 {
            firePause()
        }
    }
    
    public func onPlayerEndedEvent(_ player: ShakaPlayer) {
        delegate?.onPlayerEndedEvent?(player)
        fireStop()
    }
    
    public func onPlayerSeekingEvent(_ player: ShakaPlayer) {
        delegate?.onPlayerSeekingEvent?(player)
        if player.isLive {
            fireSeekBegin()
        }
    }

    public func onPlayerSeekedEvent(_ player: ShakaPlayer) {
        delegate?.onPlayerSeekedEvent?(player)
        if player.isLive {
            fireSeekEnd()
        }
    }
    
    public func onPlayerAttachMse(_ player: ShakaPlayer) {
        delegate?.onPlayerAttachMse?(player)
    }
    
    public func onPlayerAttachSource(_ player: ShakaPlayer) {
        delegate?.onPlayerAttachSource?(player)
        if !flags.started {
            fireStart()
        }
    }
    
    public func onPlayerDetach(_ player: ShakaPlayer) {
        delegate?.onPlayerDetach?(player)
        fireStop()
    }

}
