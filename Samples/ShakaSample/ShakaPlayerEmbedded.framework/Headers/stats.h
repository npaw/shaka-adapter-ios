// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_STATS_H_
#define SHAKA_EMBEDDED_STATS_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "macros.h"
#include "optional.h"

namespace shaka {

namespace js {

struct TrackChoice;
struct StateChange;
struct Stats;

} // namespace js

/**
 * Holds info about a track selection choice that was made.
 * @ingroup externs
 */
class SHAKA_EXPORT TrackChoice final {
 public:
  TrackChoice();
  TrackChoice(js::TrackChoice&& internal);
  TrackChoice(const TrackChoice&);
  TrackChoice(TrackChoice&&);
  ~TrackChoice();

  TrackChoice& operator=(const TrackChoice&);
  TrackChoice& operator=(TrackChoice&&);

  /**
   * The timestamp the choice was made, in seconds since 1970
   * (i.e. Date.now() / 1000).
   */
  double timestamp() const;
  /** The id of the track that was chosen. */
  double id() const;
  /** The type of track chosen ('variant' or 'text'). */
  const std::string& type() const;
  /**
   * True if the choice was made by AbrManager for adaptation; false if it
   * was made by the application through selectTrack.
   */
  bool from_adaptation() const;
  /** The bandwidth of the chosen track (null for text). */
  shaka::optional<double> bandwidth() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::TrackChoice GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};

/**
 * Holds info about a state change that occurred.
 * @ingroup externs
 */
class SHAKA_EXPORT StateChange final {
 public:
  StateChange();
  StateChange(js::StateChange&& internal);
  StateChange(const StateChange&);
  StateChange(StateChange&&);
  ~StateChange();

  StateChange& operator=(const StateChange&);
  StateChange& operator=(StateChange&&);

  /**
   * The timestamp the state was entered, in seconds since 1970
   * (i.e. Date.now() / 1000).
   */
  double timestamp() const;
  /**
   * The state the player entered.  This could be 'buffering', 'playing',
   * 'paused', or 'ended'.
   */
  const std::string& state() const;
  /**
   * The number of seconds the player was in this state.  If this is the last
   * entry in the list, the player is still in this state, so the duration will
   * continue to increase.
   */
  double duration() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::StateChange GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};

/**
 * Contains statistics and information about the current state of the player.
 * This is meant for applications that want to log quality-of-experience (QoE)
 * or other stats.  These values will reset when load() is called again.
 *
 * @ingroup externs
 */
class SHAKA_EXPORT Stats final {
 public:
  Stats();
  Stats(js::Stats&& internal);
  Stats(const Stats&);
  Stats(Stats&&);
  ~Stats();

  Stats& operator=(const Stats&);
  Stats& operator=(Stats&&);

  /** The width of the current video track. */
  double width() const;
  /** The height of the current video track. */
  double height() const;
  /** The bandwidth required for the current streams (total, in bit/sec). */
  double stream_bandwidth() const;
  /**
   * The total number of frames decoded by the Player.  This may be NaN if this
   * is not supported by the browser.
   */
  double decoded_frames() const;
  /**
   * The total number of frames dropped by the Player.  This may be NaN if this
   * is not supported by the browser.
   */
  double dropped_frames() const;
  /** The current estimated network bandwidth (in bit/sec). */
  double estimated_bandwidth() const;
  /**
   * This is the number of seconds it took for the video element to have enough
   * data to begin playback.  This is measured from the time load() is called to
   * the time the 'loadeddata' event is fired by the media element.
   */
  double load_latency() const;
  /** The total time spent in a playing state in seconds. */
  double play_time() const;
  /** The total time spent in a buffering state in seconds. */
  double buffering_time() const;
  /** A history of the stream changes. */
  const std::vector<shaka::TrackChoice>& switch_history() const;
  /** A history of the state changes. */
  const std::vector<shaka::StateChange>& state_history() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::Stats GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};


} // namespace shaka

#endif  // SHAKA_EMBEDDED_STATS_H_
