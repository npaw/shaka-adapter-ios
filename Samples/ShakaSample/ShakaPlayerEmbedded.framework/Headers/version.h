// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#include "macros.h"

#define SHAKA_VERSION_MAJOR 0ULL
#define SHAKA_VERSION_MINOR 0ULL
#define SHAKA_VERSION_REVISION 65535ULL
#define SHAKA_VERSION_TAG 8655ULL
#define SHAKA_VERSION_INT ((SHAKA_VERSION_MAJOR << 48ULL) | (SHAKA_VERSION_MINOR << 32ULL) | (SHAKA_VERSION_REVISION << 16ULL) | (SHAKA_VERSION_TAG))
#define SHAKA_VERSION_STR "v0.1.0-beta-231-ga4e739bd-dirty"

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus
/** @return The runtime version of the library. */
SHAKA_EXPORT const char* GetShakaEmbeddedVersion(void);
#ifdef __cplusplus
}
#endif  // __cplusplus
