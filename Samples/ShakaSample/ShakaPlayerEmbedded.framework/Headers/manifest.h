// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_MANIFEST_H_
#define SHAKA_EMBEDDED_MANIFEST_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "macros.h"
#include "optional.h"

namespace shaka {

namespace js {

struct InitDataOverride;
struct DrmInfo;

} // namespace js

/**
 * Explicit initialization data, which override any initialization data in the
 * content. The initDataType values and the formats that they correspond to are
 * specified here: https://bit.ly/EmeInitTypes.
 *
 * @ingroup externs
 */
class SHAKA_EXPORT InitDataOverride final {
 public:
  InitDataOverride();
  InitDataOverride(js::InitDataOverride&& internal);
  InitDataOverride(const InitDataOverride&);
  InitDataOverride(InitDataOverride&&);
  ~InitDataOverride();

  InitDataOverride& operator=(const InitDataOverride&);
  InitDataOverride& operator=(InitDataOverride&&);

  /** A string to indicate what format initData is in. */
  const std::string& init_data_type() const;
  /** The key Id that corresponds to this initData. */
  shaka::optional<std::string> key_id() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::InitDataOverride GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};

/**
 * DRM configuration for a single key system.
 *
 * @ingroup externs
 */
class SHAKA_EXPORT DrmInfo final {
 public:
  DrmInfo();
  DrmInfo(js::DrmInfo&& internal);
  DrmInfo(const DrmInfo&);
  DrmInfo(DrmInfo&&);
  ~DrmInfo();

  DrmInfo& operator=(const DrmInfo&);
  DrmInfo& operator=(DrmInfo&&);

  /** The key system, e.g., "com.widevine.alpha". */
  const std::string& key_system() const;
  /** The license server URI. */
  const std::string& license_server_uri() const;
  /**
   * True if the application requires the key system to support distinctive
   * identifiers.
   */
  bool distinctive_identifier_required() const;
  /**
   * True if the application requires the key system to support persistent
   * state, e.g., for persistent license storage.
   */
  bool persistent_state_required() const;
  /** A key-system-specific string that specifies a required security level. */
  const std::string& audio_robustness() const;
  /** A key-system-specific string that specifies a required security level. */
  const std::string& video_robustness() const;
  /**
   * A list of initialization data which override any initialization data found
   * in the content.
   */
  const std::vector<shaka::InitDataOverride>& init_data() const;
  /**
   * If not empty, contains the default key IDs for this key system, as
   * lowercase hex strings.
   */
  const std::vector<std::string>& key_ids() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::DrmInfo GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};


} // namespace shaka

#endif  // SHAKA_EMBEDDED_MANIFEST_H_
