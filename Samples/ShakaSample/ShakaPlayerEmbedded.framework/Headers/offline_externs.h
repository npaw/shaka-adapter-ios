// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OFFLINE_EXTERNS_H_
#define SHAKA_EMBEDDED_OFFLINE_EXTERNS_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "macros.h"
#include "optional.h"

namespace shaka {

namespace js {

struct StoredContent;

} // namespace js

/**
 * Contains a description of a piece of content that is stored offline.
 * @ingroup externs
 */
class SHAKA_EXPORT StoredContent final {
 public:
  StoredContent();
  StoredContent(js::StoredContent&& internal);
  StoredContent(const StoredContent&);
  StoredContent(StoredContent&&);
  ~StoredContent();

  StoredContent& operator=(const StoredContent&);
  StoredContent& operator=(StoredContent&&);

  /**
   * An offline URI to access the content. This can be passed directly to
   * Player. If the uri is null, it means that the content has not finished
   * downloading and is not ready to play.
   */
  shaka::optional<std::string> offline_uri() const;
  /** The original manifest URI of the content stored. */
  const std::string& original_manifest_uri() const;
  /** The duration of the content, in seconds. */
  double duration() const;
  /** The size of the content, in bytes. */
  double size() const;
  /**
   * The time that the encrypted license expires, in milliseconds. If the media
   * is clear or the license never expires, this will equal Infinity.
   */
  double expiration() const;
  /** The metadata passed to store. */
  const std::unordered_map<std::string, std::string>& app_metadata() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::StoredContent GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};


} // namespace shaka

#endif  // SHAKA_EMBEDDED_OFFLINE_EXTERNS_H_
