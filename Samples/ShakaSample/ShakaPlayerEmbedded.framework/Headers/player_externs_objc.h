// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OBJC_PLAYER_EXTERNS_H_
#define SHAKA_EMBEDDED_OBJC_PLAYER_EXTERNS_H_

#import <Foundation/Foundation.h>

#include "macros.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Contains the times of a range of buffered content.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaBufferedRange : NSObject

/** The start time of the range, in seconds. */
@property (atomic, readonly) double start;

/** The end time of the range, in seconds. */
@property (atomic, readonly) double end;

@end


/**
 * Contains information about the current buffered ranges.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaBufferedInfo : NSObject

/** The combined audio/video buffered ranges, reported by |video.buffered|. */
@property (atomic, readonly) NSArray<ShakaBufferedRange *> *total;

/** The buffered ranges for audio content. */
@property (atomic, readonly) NSArray<ShakaBufferedRange *> *audio;

/** The buffered ranges for video content. */
@property (atomic, readonly) NSArray<ShakaBufferedRange *> *video;

/** The buffered ranges for text content. */
@property (atomic, readonly) NSArray<ShakaBufferedRange *> *text;

@end


/**
 * A pair of language and role.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaLanguageRole : NSObject

/** The language code for the stream. */
@property (atomic, readonly) NSString *language;

/**
 * The role name for the stream. If the stream has no role, |role| will be ''.
 */
@property (atomic, readonly) NSString *role;

@end


NS_ASSUME_NONNULL_END
#endif  // SHAKA_EMBEDDED_OBJC_PLAYER_EXTERNS_H_
