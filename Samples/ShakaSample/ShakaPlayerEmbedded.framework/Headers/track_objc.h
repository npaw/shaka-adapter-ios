// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OBJC_TRACK_H_
#define SHAKA_EMBEDDED_OBJC_TRACK_H_

#import <Foundation/Foundation.h>

#include "macros.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * An object describing a media track.  This object should be treated as
 * read-only as changing any values does not have any effect.  This is the
 * public view of an audio/video paring (variant type) or text track (text
 * type).
 *
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaTrack : NSObject

/** The unique ID of the track. */
@property (atomic, readonly) double id;

/**
 * If true, this is the track being streamed (another track may be
 * visible/audible in the buffer).
 */
@property (atomic, readonly) BOOL active;

/** The type of track, either 'variant' or 'text'. */
@property (atomic, readonly) NSString *type;

/** The bandwidth required to play the track, in bits/sec. */
@property (atomic, readonly) double bandwidth;

/**
 * The language of the track, or 'und' if not given.  This is the exact
 * value provided in the manifest; it may need to be normalized.
 */
@property (atomic, readonly) NSString *language;

/** The track label, which is unique text that should describe the track. */
@property (atomic, readonly, nullable) NSString *label;

/**
 * (only for text tracks) The kind of text track, either 'caption' or
 * 'subtitle'.
 */
@property (atomic, readonly, nullable) NSString *kind;

/** The video width provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSNumber *width;

/** The video height provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSNumber *height;

/** The video framerate provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSNumber *frameRate;

/** The MIME type of the content provided in the manifest. */
@property (atomic, readonly, nullable) NSString *mimeType;

/** The audio/video codecs string provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSString *codecs;

/** The audio codecs string provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSString *audioCodec;

/** The video codecs string provided in the manifest, if present. */
@property (atomic, readonly, nullable) NSString *videoCodec;

/**
 * True indicates that this in the primary language for the content.
 * This flag is based on signals from the manifest.
 * This can be a useful hint about which language should be the default, and
 * indicates which track Shaka will use when the user's language preference
 * cannot be satisfied.
 */
@property (atomic, readonly) BOOL primary;

/** The roles of the track, e.g. 'main', 'caption', or 'commentary'. */
@property (atomic, readonly) NSArray<NSString *> *roles;

/** (only for variant tracks) The video stream id. */
@property (atomic, readonly, nullable) NSNumber *videoId;

/** (only for variant tracks) The audio stream id. */
@property (atomic, readonly, nullable) NSNumber *audioId;

/** The count of the audio track channels. */
@property (atomic, readonly, nullable) NSNumber *channelsCount;

/** (only for variant tracks) The audio stream's bandwidth if known. */
@property (atomic, readonly, nullable) NSNumber *audioBandwidth;

/** (only for variant tracks) The video stream's bandwidth if known. */
@property (atomic, readonly, nullable) NSNumber *videoBandwidth;

@end


NS_ASSUME_NONNULL_END
#endif  // SHAKA_EMBEDDED_OBJC_TRACK_H_
