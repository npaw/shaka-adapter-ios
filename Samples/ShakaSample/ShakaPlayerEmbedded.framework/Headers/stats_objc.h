// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OBJC_STATS_H_
#define SHAKA_EMBEDDED_OBJC_STATS_H_

#import <Foundation/Foundation.h>

#include "macros.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Holds info about a track selection choice that was made.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaTrackChoice : NSObject

/**
 * The timestamp the choice was made, in seconds since 1970
 * (i.e. Date.now() / 1000).
 */
@property (atomic, readonly) double timestamp;

/** The id of the track that was chosen. */
@property (atomic, readonly) double id;

/** The type of track chosen ('variant' or 'text'). */
@property (atomic, readonly) NSString *type;

/**
 * True if the choice was made by AbrManager for adaptation; false if it
 * was made by the application through selectTrack.
 */
@property (atomic, readonly) BOOL fromAdaptation;

/** The bandwidth of the chosen track (null for text). */
@property (atomic, readonly, nullable) NSNumber *bandwidth;

@end


/**
 * Holds info about a state change that occurred.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaStateChange : NSObject

/**
 * The timestamp the state was entered, in seconds since 1970
 * (i.e. Date.now() / 1000).
 */
@property (atomic, readonly) double timestamp;

/**
 * The state the player entered.  This could be 'buffering', 'playing',
 * 'paused', or 'ended'.
 */
@property (atomic, readonly) NSString *state;

/**
 * The number of seconds the player was in this state.  If this is the last
 * entry in the list, the player is still in this state, so the duration will
 * continue to increase.
 */
@property (atomic, readonly) double duration;

@end


/**
 * Contains statistics and information about the current state of the player.
 * This is meant for applications that want to log quality-of-experience (QoE)
 * or other stats.  These values will reset when load() is called again.
 *
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaStats : NSObject

/** The width of the current video track. */
@property (atomic, readonly) double width;

/** The height of the current video track. */
@property (atomic, readonly) double height;

/** The bandwidth required for the current streams (total, in bit/sec). */
@property (atomic, readonly) double streamBandwidth;

/**
 * The total number of frames decoded by the Player.  This may be NaN if this
 * is not supported by the browser.
 */
@property (atomic, readonly) double decodedFrames;

/**
 * The total number of frames dropped by the Player.  This may be NaN if this
 * is not supported by the browser.
 */
@property (atomic, readonly) double droppedFrames;

/** The current estimated network bandwidth (in bit/sec). */
@property (atomic, readonly) double estimatedBandwidth;

/**
 * This is the number of seconds it took for the video element to have enough
 * data to begin playback.  This is measured from the time load() is called to
 * the time the 'loadeddata' event is fired by the media element.
 */
@property (atomic, readonly) double loadLatency;

/** The total time spent in a playing state in seconds. */
@property (atomic, readonly) double playTime;

/** The total time spent in a buffering state in seconds. */
@property (atomic, readonly) double bufferingTime;

/** A history of the stream changes. */
@property (atomic, readonly) NSArray<ShakaTrackChoice *> *switchHistory;

/** A history of the state changes. */
@property (atomic, readonly) NSArray<ShakaStateChange *> *stateHistory;

@end


NS_ASSUME_NONNULL_END
#endif  // SHAKA_EMBEDDED_OBJC_STATS_H_
