// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_PLAYER_EXTERNS_H_
#define SHAKA_EMBEDDED_PLAYER_EXTERNS_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "macros.h"
#include "optional.h"

namespace shaka {

namespace js {

struct BufferedRange;
struct BufferedInfo;
struct LanguageRole;

} // namespace js

/**
 * Contains the times of a range of buffered content.
 * @ingroup externs
 */
class SHAKA_EXPORT BufferedRange final {
 public:
  BufferedRange();
  BufferedRange(js::BufferedRange&& internal);
  BufferedRange(const BufferedRange&);
  BufferedRange(BufferedRange&&);
  ~BufferedRange();

  BufferedRange& operator=(const BufferedRange&);
  BufferedRange& operator=(BufferedRange&&);

  /** The start time of the range, in seconds. */
  double start() const;
  /** The end time of the range, in seconds. */
  double end() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::BufferedRange GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};

/**
 * Contains information about the current buffered ranges.
 * @ingroup externs
 */
class SHAKA_EXPORT BufferedInfo final {
 public:
  BufferedInfo();
  BufferedInfo(js::BufferedInfo&& internal);
  BufferedInfo(const BufferedInfo&);
  BufferedInfo(BufferedInfo&&);
  ~BufferedInfo();

  BufferedInfo& operator=(const BufferedInfo&);
  BufferedInfo& operator=(BufferedInfo&&);

  /** The combined audio/video buffered ranges, reported by |video.buffered|. */
  const std::vector<shaka::BufferedRange>& total() const;
  /** The buffered ranges for audio content. */
  const std::vector<shaka::BufferedRange>& audio() const;
  /** The buffered ranges for video content. */
  const std::vector<shaka::BufferedRange>& video() const;
  /** The buffered ranges for text content. */
  const std::vector<shaka::BufferedRange>& text() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::BufferedInfo GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};

/**
 * A pair of language and role.
 * @ingroup externs
 */
class SHAKA_EXPORT LanguageRole final {
 public:
  LanguageRole();
  LanguageRole(js::LanguageRole&& internal);
  LanguageRole(const LanguageRole&);
  LanguageRole(LanguageRole&&);
  ~LanguageRole();

  LanguageRole& operator=(const LanguageRole&);
  LanguageRole& operator=(LanguageRole&&);

  /** The language code for the stream. */
  const std::string& language() const;
  /**
   * The role name for the stream. If the stream has no role, |role| will be ''.
   */
  const std::string& role() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::LanguageRole GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};


} // namespace shaka

#endif  // SHAKA_EMBEDDED_PLAYER_EXTERNS_H_
