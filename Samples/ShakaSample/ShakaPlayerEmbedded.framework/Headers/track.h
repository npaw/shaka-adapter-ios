// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_TRACK_H_
#define SHAKA_EMBEDDED_TRACK_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "macros.h"
#include "optional.h"

namespace shaka {

namespace js {

struct Track;

} // namespace js

/**
 * An object describing a media track.  This object should be treated as
 * read-only as changing any values does not have any effect.  This is the
 * public view of an audio/video paring (variant type) or text track (text
 * type).
 *
 * @ingroup externs
 */
class SHAKA_EXPORT Track final {
 public:
  Track();
  Track(js::Track&& internal);
  Track(const Track&);
  Track(Track&&);
  ~Track();

  Track& operator=(const Track&);
  Track& operator=(Track&&);

  /** The unique ID of the track. */
  double id() const;
  /**
   * If true, this is the track being streamed (another track may be
   * visible/audible in the buffer).
   */
  bool active() const;
  /** The type of track, either 'variant' or 'text'. */
  const std::string& type() const;
  /** The bandwidth required to play the track, in bits/sec. */
  double bandwidth() const;
  /**
   * The language of the track, or 'und' if not given.  This is the exact
   * value provided in the manifest; it may need to be normalized.
   */
  const std::string& language() const;
  /** The track label, which is unique text that should describe the track. */
  shaka::optional<std::string> label() const;
  /**
   * (only for text tracks) The kind of text track, either 'caption' or
   * 'subtitle'.
   */
  shaka::optional<std::string> kind() const;
  /** The video width provided in the manifest, if present. */
  shaka::optional<double> width() const;
  /** The video height provided in the manifest, if present. */
  shaka::optional<double> height() const;
  /** The video framerate provided in the manifest, if present. */
  shaka::optional<double> frame_rate() const;
  /** The MIME type of the content provided in the manifest. */
  shaka::optional<std::string> mime_type() const;
  /** The audio/video codecs string provided in the manifest, if present. */
  shaka::optional<std::string> codecs() const;
  /** The audio codecs string provided in the manifest, if present. */
  shaka::optional<std::string> audio_codec() const;
  /** The video codecs string provided in the manifest, if present. */
  shaka::optional<std::string> video_codec() const;
  /**
   * True indicates that this in the primary language for the content.
   * This flag is based on signals from the manifest.
   * This can be a useful hint about which language should be the default, and
   * indicates which track Shaka will use when the user's language preference
   * cannot be satisfied.
   */
  bool primary() const;
  /** The roles of the track, e.g. 'main', 'caption', or 'commentary'. */
  const std::vector<std::string>& roles() const;
  /** (only for variant tracks) The video stream id. */
  shaka::optional<double> video_id() const;
  /** (only for variant tracks) The audio stream id. */
  shaka::optional<double> audio_id() const;
  /** The count of the audio track channels. */
  shaka::optional<double> channels_count() const;
  /** (only for variant tracks) The audio stream's bandwidth if known. */
  shaka::optional<double> audio_bandwidth() const;
  /** (only for variant tracks) The video stream's bandwidth if known. */
  shaka::optional<double> video_bandwidth() const;

  /** INTERNAL USE ONLY: Get the internal representation of this object. */
  js::Track GetInternal() const;

 private:
  class Impl;
  std::shared_ptr<Impl> impl_;
};


} // namespace shaka

#endif  // SHAKA_EMBEDDED_TRACK_H_
