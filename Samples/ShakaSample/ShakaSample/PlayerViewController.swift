//
//  PlayerViewController.swift
//  ShakaSample
//
//  Created by Elisabet Massó on 4/3/21.
//

import UIKit
import YouboraLib
import YouboraConfigUtils
import ShakaPlayerEmbedded
import YouboraShakaAdapter
import YouboraAVPlayerAdapter

class PlayerViewController: UIViewController {
    
    var resource: String?

    var plugin: YBPlugin?
    
    var player: ShakaPlayer?
    
    var playerView: ShakaPlayerView?

    var timer: Timer?
    var backgroundTime: Double?
    
    var isTouchingSlider = false
    
    var changeItemButton = UIButton(type: .custom)
    var buttonReplay = UIButton(type: .custom)
    var playerContainer = UIView()
    
    private let controlBtn: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "pause")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleControls), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private let leftTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let currentTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let videoSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.red
        slider.maximumTrackTintColor = UIColor.white
        slider.thumbTintColor = UIColor.red
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(handleSliderValueChanged), for: [.touchUpInside, .touchDown])
        slider.isHidden = true
        return slider
    }()
    
    private let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initializeYoubora()
        initializePlayer()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let audioSession = AVAudioSession.sharedInstance()

        do {
            try audioSession.setCategory(.playback)
            try audioSession.setActive(true)
        }
        catch {
          print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            timer?.invalidate()
            timer = nil
            plugin?.fireStop()
            plugin?.removeAdapter()
            player?.unload { (error) in
                self.playerView?.removeFromSuperview()
                self.player = nil
            }
        }
        
    }
    
    private func initializeYoubora() {
        YBLog.setDebugLevel(.debug)
        
        let options = YouboraConfigManager.getOptions()
        options.contentIsLive = NSNumber(value: resource == Resource.live || resource == Resource.liveAirShow)
        options.contentResource = resource
        plugin = YBPlugin(options: options)
    }
    
    private func configureControls() {
        changeItemButton.setTitle("Change Item", for: .normal)
        changeItemButton.addTarget(self, action: #selector(changeItem), for:.touchUpInside)
        changeItemButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(changeItemButton)
        NSLayoutConstraint.activate([
            changeItemButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100),
            changeItemButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            changeItemButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            changeItemButton.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        buttonReplay.setTitle("Replay", for: .normal)
        buttonReplay.addTarget(self, action: #selector(pressToReply), for:.touchUpInside)
        buttonReplay.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(buttonReplay)
        NSLayoutConstraint.activate([
            buttonReplay.topAnchor.constraint(equalTo: changeItemButton.bottomAnchor, constant: 20),
            buttonReplay.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            buttonReplay.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            buttonReplay.heightAnchor.constraint(equalToConstant: 20)
        ])
        buttonReplay.isHidden = true
        
        playerContainer.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(playerContainer)
        NSLayoutConstraint.activate([
            playerContainer.topAnchor.constraint(equalTo: buttonReplay.bottomAnchor, constant: 0),
            playerContainer.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            playerContainer.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            playerContainer.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])

        container.frame = view.bounds
        view.addSubview(container)
        container.topAnchor.constraint(equalTo: playerContainer.topAnchor, constant: 0).isActive = true
        container.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        container.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 9/16).isActive = true
        
        controlBtn.frame = container.bounds
        controlBtn.bounds = controlBtn.frame.insetBy(dx: 20.0, dy: 20.0)
        container.addSubview(controlBtn)
        controlBtn.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10).isActive = true
        controlBtn.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8).isActive = true
        controlBtn.widthAnchor.constraint(equalToConstant: 15).isActive = true
        controlBtn.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        if let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) {
            buttonReplay.isHidden = false
            leftTimeLbl.frame = container.bounds
            container.addSubview(leftTimeLbl)
            leftTimeLbl.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -10).isActive = true
            leftTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            leftTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            leftTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            leftTimeLbl.isHidden = false

            currentTimeLbl.frame = container.bounds
            container.addSubview(currentTimeLbl)
            currentTimeLbl.leftAnchor.constraint(equalTo: controlBtn.rightAnchor, constant: 10).isActive = true
            currentTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            currentTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
            currentTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            currentTimeLbl.isHidden = false

            videoSlider.frame = container.bounds
            container.addSubview(videoSlider)
            videoSlider.rightAnchor.constraint(equalTo: leftTimeLbl.leftAnchor).isActive = true
            videoSlider.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
            videoSlider.leftAnchor.constraint(equalTo: currentTimeLbl.rightAnchor).isActive = true
            videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
            videoSlider.isHidden = false
        }
        
        self.view.layoutIfNeeded()
    }
    
    @objc private func handleControls() {
        if let isPaused = player?.paused, isPaused {
            player?.play()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        } else if let ended = player?.ended, ended {
            player?.play()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            player?.pause()
            controlBtn.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    @objc private func handleSliderValueChanged(sender: UISlider) {
        if player?.avPlayer != nil, let totalSeconds = player?.duration {
            let value = Double(videoSlider.value) * totalSeconds
            player?.currentTime = value
        } else if let player = player {
            player.getUiInfo { (info) in
                let value = Double(self.videoSlider.value) * player.duration
                player.currentTime = value
                self.isTouchingSlider = !self.isTouchingSlider
            }
        }
    }
    
    @objc func handleTime() {
        if let currentTime = player?.currentTime, let duration = player?.duration {
            currentTimeLbl.text = "\(getTextTimeFrom(time: currentTime))"
            leftTimeLbl.text = "-\(getTextTimeFrom(time: duration - currentTime))"
            if !isTouchingSlider {
                videoSlider.value = Float(currentTime / duration)
            }
        }
    }
    
    @objc func changeItem() {
        guard let newResource = [Resource.dashBitmovin, Resource.dashShaka].randomElement(), let player = player else {
            return
        }
        plugin?.options.contentIsLive = NSNumber(false)
        plugin?.options.contentResource = newResource
        
        configureControls()
        
        player.unload { (error) in
            if let error = error {
                print("Error loading manifest: \(error.message)")
                /// This error does not arrive by `client` delegate so it must be called manually
                guard let adapter = self.plugin?.adapter as? YBShakaAdapter else { return }
                adapter.onPlayer(player, error: error)
            } else {
                self.loadPlayer(withResource: newResource)
            }
        }
        
    }
    
    func loadPlayer(withResource resource: String, at startTime: Double? = nil) {
        guard let player = player else { return }
        playerView?.removeFromSuperview()
        
        playerView = ShakaPlayerView(player: player)
        playerView!.frame = self.view.bounds
        self.view.addSubview(playerView!)
        configureControls()
        
        player.load(resource, withStartTime: startTime ?? 0) { (error) in
            if let error = error {
                print("Error loading manifest: \(error.message)")
                /// This error does not arrive by `client` delegate so it must be called manually
                guard let adapter = self.plugin?.adapter as? YBShakaAdapter else { return }
                adapter.onPlayer(player, error: error)
            } else {
                player.play()
            }
        }
        
        handleControls()
    }
    
    @objc func pressToReply() {
        player?.currentTime = 0
    }
    
    private func getTextTimeFrom(time totalSeconds: Double) -> String {
        if !totalSeconds.isNaN && !totalSeconds.isInfinite {
            let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
            let secondsText = String(format: "%02d", seconds)
            let minutesText = String(format: "%02d", Int(totalSeconds / 60))
            return "\(minutesText):\(secondsText)"
        }
        return ""
    }
    
    @objc func didEnterBackground(_ notification: Notification?) {
        guard let player = player else { return }
        playerView?.player = nil
        playerView = nil
        backgroundTime = player.currentTime
        player.unload { (error) in
            if let error = error {
                print("Error loading manifest: \(error.message)")
                /// This error does not arrive by `client` delegate so it must be called manually
                guard let adapter = self.plugin?.adapter as? YBShakaAdapter else { return }
                adapter.onPlayer(player, error: error)
            }
        }
    }
    
    @objc func didEnterForeground(_ notification: Notification?) {
        guard let resource = resource else { return }
        loadPlayer(withResource: resource, at: self.backgroundTime)
        backgroundTime = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

// MARK: - Video

extension PlayerViewController {
    
    func initializePlayer() {
        // Make a Shaka Player with its corresponding view.
        guard let player = try? ShakaPlayer(), let resource = resource else {
            print("Error creating player")
            return
        }
        
        self.player = player
        
        player.client = self
        
        plugin?.adapter = YBShakaAdapter(player: player, client: self)
        
        // Load and play an asset.
        loadPlayer(withResource: resource)
        plugin?.fireInit() // Should send fireInit as the events only start when player starts to play
    }
    
}

extension PlayerViewController: ShakaPlayerClient {
    
    func onPlayer(_ player: ShakaPlayer, error: ShakaPlayerError) {
        print("Got Shaka Player Error: \(error.message)")
    }
    
    func onPlayerPlayingEvent(_ player: ShakaPlayer) {
        print("Got Shaka Player Playing")
        controlBtn.isHidden = false
        if player.isLive {
            leftTimeLbl.isHidden = false
            currentTimeLbl.isHidden = false
            videoSlider.isHidden = false
        } else if let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) {
            if timer == nil {
                timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(handleTime), userInfo: nil, repeats: true)
            }
        }
    }
    
    func onPlayerEndedEvent(_ player: ShakaPlayer) {
        print("Got Shaka Player Ended")
        controlBtn.setImage(UIImage(named: "resume"), for: .normal)
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    func onPlayerAttachSource(_ player: ShakaPlayer) {
        initializeAdapterIfNeeded()
    }
    
    func onPlayerDetach(_ player: ShakaPlayer) {
        initializeAdapterIfNeeded()
    }
    
    func onPlayerAttachMse(_ player: ShakaPlayer) {
        initializeAdapterIfNeeded()
    }
    
    func initializeAdapterIfNeeded() {
        if let adapter = plugin?.adapter, adapter.isKind(of: YBShakaAdapter.self), let player = player, let avPlayer = player.avPlayer {
            player.client = self
            plugin?.adapter = YBAVPlayerAdapterSwiftTranformer.transform(from: YBAVPlayerAdapter(player: avPlayer))
        } else if let adapter = plugin?.adapter, adapter.isKind(of: YBAVPlayerAdapter.self), let player = player, player.avPlayer == nil {
            player.client = self
            plugin?.adapter = YBShakaAdapter(player: player, client: self)
            plugin?.fireInit() // Should send fireInit as the events only start when player starts to play
        }
    }
    
}
