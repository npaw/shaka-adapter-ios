# YouboraShakaAdapter #

A framework that will collect several video events from the ShakaPlayer and send it to the back end

# Installation #

#### Cloning the repo ####

In shell located in your project folder run

```bash
git clone https://bitbucket.org/npaw/shaka-adapter-ios.git
```

add the target in your Podfile

```bash
target 'YouboraShakaAdapter' do
	project 'shaka-adapter-ios/YouboraShakaAdapter.xcodeproj'
	use_frameworks!
	# Pods for YouboraShakaAdapter
	pod 'YouboraLib', '~> 6.5'
end
```

then run

```bash
pod install
```

and then:

* Select the project file in the "Project" navigator column, and the target in the project and targets list
* In the "General" tab scroll down to the "Linked Frameworks and Libraries" section and click the "+" button
* Now look for the YouboraShakaAdapter.framework in the options, select it and press "Add"

Then you will need to go to **{YOUR_SCHEME} > Build Settings > Enable Bitcode** and disable it in order to build ShakaPlayerEmbedded framework

## How to use ##

## Start plugin and options ##

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
    let options = YBOptions()
    options.contentResource = "http://example.com"
    options.accountCode = "accountCode"
    options.adResource = "http://example.com"
    options.contentIsLive = NSNumber(value: false)
    return options;
}
    
lazy var plugin = YBPlugin(options: self.options)
```

### YBShakaAdapter ###

```swift
import YouboraShakaAdapter

...

//Once you have your player and plugin initialized you can set the adapter
guard let player = try? ShakaPlayer() else {
  return
}

plugin.adapter = YBShakaAdapter(player: player, client: self)

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
plugin.fireStop()
plugin.removeAdapter()
```

#### Errors handling ####

The adapter cannot track the errors in load and unload player closures. It is possible to call this method from the adapter to track it:

```swift
adapter.onPlayer(player, error: error)
```

or calling the method from the plugin to customize it:

```swift
plugin?.fireFatalError(withMessage: errorMsg, code: errorCode, andErrorMetadata: errorMeta, andException: errException)
plugin?.fireError(withMessage: errorMsg, code: errorCode, andErrorMetadata: errorMeta)
```

## Run samples project ##

###### Via the cloned repo ######

Once the repo is cloned open the ```shaka-adapter-ios``` folder and run

```bash
pod install
```

The project should be ready now to work

