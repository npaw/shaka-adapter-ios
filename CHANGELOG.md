## [6.6.0] - 2022-03-02
### Added
- Seek events for live content

### Fixed
- Xcode 13 framework version
 
## [6.5.0] - 2021-04-15
### Added
- Releae version
 
