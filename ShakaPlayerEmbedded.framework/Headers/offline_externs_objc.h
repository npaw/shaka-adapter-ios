// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OBJC_OFFLINE_EXTERNS_H_
#define SHAKA_EMBEDDED_OBJC_OFFLINE_EXTERNS_H_

#import <Foundation/Foundation.h>

#include "macros.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Contains a description of a piece of content that is stored offline.
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaStoredContent : NSObject

/**
 * An offline URI to access the content. This can be passed directly to
 * Player. If the uri is null, it means that the content has not finished
 * downloading and is not ready to play.
 */
@property (atomic, readonly, nullable) NSString *offlineUri;

/** The original manifest URI of the content stored. */
@property (atomic, readonly) NSString *originalManifestUri;

/** The duration of the content, in seconds. */
@property (atomic, readonly) double duration;

/** The size of the content, in bytes. */
@property (atomic, readonly) double size;

/**
 * The time that the encrypted license expires, in milliseconds. If the media
 * is clear or the license never expires, this will equal Infinity.
 */
@property (atomic, readonly) double expiration;

/** The metadata passed to store. */
@property (atomic, readonly) NSDictionary<NSString *, NSString *> *appMetadata;

@end


NS_ASSUME_NONNULL_END
#endif  // SHAKA_EMBEDDED_OBJC_OFFLINE_EXTERNS_H_
