// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// AUTO-GENERATED: DO NOT EDIT

#ifndef SHAKA_EMBEDDED_OBJC_MANIFEST_H_
#define SHAKA_EMBEDDED_OBJC_MANIFEST_H_

#import <Foundation/Foundation.h>

#include "macros.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Explicit initialization data, which override any initialization data in the
 * content. The initDataType values and the formats that they correspond to are
 * specified here: https://bit.ly/EmeInitTypes.
 *
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaInitDataOverride : NSObject

/** A string to indicate what format initData is in. */
@property (atomic, readonly) NSString *getInitDataType;

/** The key Id that corresponds to this initData. */
@property (atomic, readonly, nullable) NSString *keyId;

@end


/**
 * DRM configuration for a single key system.
 *
 * @ingroup externs
 */
SHAKA_EXPORT
@interface ShakaDrmInfo : NSObject

/** The key system, e.g., "com.widevine.alpha". */
@property (atomic, readonly) NSString *keySystem;

/** The license server URI. */
@property (atomic, readonly) NSString *licenseServerUri;

/**
 * True if the application requires the key system to support distinctive
 * identifiers.
 */
@property (atomic, readonly) BOOL distinctiveIdentifierRequired;

/**
 * True if the application requires the key system to support persistent
 * state, e.g., for persistent license storage.
 */
@property (atomic, readonly) BOOL persistentStateRequired;

/** A key-system-specific string that specifies a required security level. */
@property (atomic, readonly) NSString *audioRobustness;

/** A key-system-specific string that specifies a required security level. */
@property (atomic, readonly) NSString *videoRobustness;

/**
 * A list of initialization data which override any initialization data found
 * in the content.
 */
@property (atomic, readonly) NSArray<ShakaInitDataOverride *> *getInitData;

/**
 * If not empty, contains the default key IDs for this key system, as
 * lowercase hex strings.
 */
@property (atomic, readonly) NSArray<NSString *> *keyIds;

@end


NS_ASSUME_NONNULL_END
#endif  // SHAKA_EMBEDDED_OBJC_MANIFEST_H_
