Pod::Spec.new do |s|

  s.name         = 'YouboraShakaAdapter'
  s.version      = '6.6.0'

  # Metadata
  s.summary      = 'Adapter to use YouboraLib on ShakaPlayer'

  s.description  = <<-DESC
                        YouboraShakaAdapter is an adapter used for ShakaPlayer.
                      DESC

  s.homepage     = 'http://developer.nicepeopleatwork.com/'

  s.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  s.author       = { 'Nice People at Work' => 'support@nicepeopleatwork.com' }

  # Platforms
  s.ios.deployment_target = '9.0'
  # s.osx.deployment_target = '10.10'
  # s.tvos.deployment_target = '9.0'

  s.swift_version = '4.0', '4.1', '4.2', '4.3', '5.0', '5.1', '5.2', '5.3'

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/shaka-adapter-ios.git', :tag => s.version }

  # Source files
  s.source_files  = 'YouboraShakaAdapter/**/*.{h,m,swift}'
  # s.ios.source_files = 'YouboraShakaAdapter/YouboraShakaAdapter\ iOS/YouboraShakaAdapter.h'
  # s.tvos.source_files = 'YouboraShakaAdapter/YouboraShakaAdapter\ tvOS/YouboraShakaAdapter.h'
  # s.osx.source_files = 'YouboraShakaAdapter/YouboraShakaAdapter\ OSX/YouboraShakaAdapter.h'
  
  # s.public_header_files = 'YouboraShakaAdapter/**/*.h'


  # Project settings
  s.requires_arc = true
  s.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORASHAKAADAPTER_VERSION=' + s.version.to_s }
  
  # Dependency
  s.dependency 'YouboraLib', '~> 6.5'

end
